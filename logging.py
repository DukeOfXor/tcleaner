# Copyright (C) 2016  DukeOfXor
#
# This file is part of Tcleaner
#
# Tcleaner is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Logger:
    def __init__(self, verbose):
        self.verbose = verbose

    def write_message(self, message):
        try:
            print(message)
        except Exception:
            self.log_warning("Exception while logging. This could be because your terminal does not support a specific charset")

    def log_info(self, message, verbose=False):
        if verbose:
            if self.verbose:
                self.write_message("[INFO] %s" % message)
        else:
            self.write_message("[INFO] %s" % message)

    def log_warning(self, message, verbose=False):
        if verbose:
            if self.verbose:
                self.write_message("[WARNING] %s" % message)
        else:
            self.write_message("[WARNING] %s" % message)
            
        
    def log_error(self, message):
 
            self.write_message("[ERROR] %s" % message)

    def log_fatal(self, message):
            self.write_message("[FATAL] %s" % message)
