# Copyright (C) 2016  DukeOfXor
#
# This file is part of Tcleaner
#
# Tcleaner is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import getopt
import sys

from tcleaner import Tcleaner

verbose = False
directory = None
help = False
extension = "tcleanerzip"
size = 100

def usage():
    name = sys.argv[0]
    print("Usage: %s [OPTIONS] --directory=directory" % name)
    print("Mandatory:")
    print(" -d, --directory=directory          Set the directory which needs cleaning")
    print("Optional:")
    print(" -h, --help                         Print this message")
    print(" -v, --verbose                      More logging")
    print(" -e, --extension=extension          Create zip files with this extension [default=\"tcleanerzip\"]")
    print(" -s, --size=size                    Files >= this size(MB) will be compressed [default=100]")

def set_options():
    global verbose
    global directory
    global help
    global extension
    global size

    try:
        options, remainder = getopt.getopt(sys.argv[1:], "vd:he:s:", ["verbose", "directory=", "help", "extension=", "size="])

        for opt, arg in options:
            if opt in ("-v", "--verbose"):
                verbose = True
            elif opt in ("-d", "--directory"):
                directory = arg
            elif opt in ("-h", "--help"):
                help = True
            elif opt in ("-e", "--extension"):
                extension = arg
            elif opt in ("-s", "--size"):
                size = arg
    except Exception:
        usage()
        exit()

def validate_options():
    global help
    
    # validate directory
    if not directory:
        help = True

    # validate size
    global size
    try:
        size = float(size)
    except ValueError:
        help = True

def execute():
    if help:
        usage()
        exit()
    
    tcleaner = Tcleaner(directory, verbose, extension, size)
    tcleaner.run()

def main():
    set_options()
    validate_options()
    execute()

if __name__ == "__main__":
    main()
