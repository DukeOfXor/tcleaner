# Copyright (C) 2016  DukeOfXor
#
# This file is part of Tcleaner
#
# Tcleaner is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import tempfile
import shutil
import zipfile
import random
import time

from logging import Logger

class Tcleaner:
    def __init__(self, directory, verbose, extension, size):
        self.directory = directory
        self.logger = Logger(verbose)
        self.extension = extension
        self.size = size
        self.validate_arguments()

    def exit(self, errors = []):
        for error in errors:
            self.logger.log_fatal(error)

        self.logger.log_info("Exiting...")
        exit()

    def validate_arguments(self):
        errors = []
        # validate directory
        if not os.path.isdir(self.directory):
            errors.append("Invalid directory")

        # validate extension
        if not self.extension.startswith("."):
            self.extension = "." + self.extension
        if len(self.extension) < 2:
            errors.append("An extension should be longer than 2 chars")

        # validate size
        if self.size < 50:
            errors.append("Size needs to be bigger than 49")
    
        if len(errors) > 0:
            self.exit(errors)

            
    def rm_dirs(self):
        removed_dirs = 0
        try:
            for subdir, dirs, file in os.walk(self.directory):
                try:
                    os.rmdir(subdir)
                    self.logger.log_info("Removed %s" % subdir, verbose=True)
                    removed_dirs += 1
                except OSError as ex:
                    # gets thrown if a dir is not empty - ignore
                    pass
            if removed_dirs > 0:
                removed_dirs += self.rm_dirs()
        except Exception as e:
            self.logger.log_error("Error while removing directories")
            raise e
        return removed_dirs

    def is_tcleaner_file(self, f):
        filename, file_extension = os.path.splitext(f)
        if file_extension == self.extension:
            return True
        return False

    def cache_file(self, f_path):
        tmp_dir = tempfile.gettempdir()
        name = os.path.basename(f_path)
        cache_file_path = os.path.join(tmp_dir, name)
        cache_file = open(cache_file_path, "wb")
        f = open(f_path, "rb")
        shutil.copyfileobj(f, cache_file)
        cache_file.close()
        f.close()
        os.remove(f_path)
        return cache_file_path

    def zip_file(self, f_in_path, f_zip_path):
        with zipfile.ZipFile(f_zip_path, "w", zipfile.ZIP_DEFLATED) as f_zip:
            f_zip.write(f_in_path, os.path.basename(f_in_path))

    def restore_file(self, f_path, f_cache_path, f_zip_path):
        # copy file from cache back to folder
        f_cache = open(f_cache_path, "rb")
        f = open(f_path, "wb")
        shutil.copyfileobj(f_cache, f)
        f_cache.close()
        f.close()
        # remove cached file and zip file
        os.remove(f_zip_path)
        os.remove(f_cache_path)

    def compress_files(self):
        compressed_files = 0
        for subdir, dirs, files in os.walk(self.directory):
            for f in files:
                f_path = os.path.join(subdir, f)
                # file needs to be bigger than self.size
                if not (os.path.getsize(f_path) / 1024) / 1024 >= self.size:
                    continue
                # ingore tcleaner files
                if self.is_tcleaner_file(f_path):
                    continue
                
                #cache file
                f_cache_path = self.cache_file(f_path)

                # zip file from cache to real path
                f_zip_path = f_path + self.extension
                try:
                    self.logger.log_info("Compressing %s" % f_path, verbose=True)
                    self.zip_file(f_cache_path, f_zip_path)
                    os.remove(f_cache_path)
                except Exception:
                    self.logger.log_warning("Error while compressing %s" % f_path)
                    try:
                        # try to restore our file and clean everything up
                        self.logger.log_info("Trying to restore %s" % f_path)
                        self.restore_file(f_path, f_cache_path, f_zip_path)
                        self.logger.log_info("Restored %s" % f_path)
                        compressed_files -= 1
                    except Exception as e:
                        self.logger.log_error("Failed to restore %s" % f_path)
                except KeyboardInterrupt:
                    # user pressed ctrl+c
                    self.logger.log_warning("KeyboardInterruption")
                    try:
                        # try to restore our file and clean everything up
                        self.logger.log_info("Trying to restore %s" % f_path)
                        self.restore_file(f_path, f_cache_path, f_zip_path)
                        self.logger.log_info("Restored %s" % f_path)
                        compressed_files -= 1
                    except Exception as e:
                        self.logger.log_error("Failed to restore %s" % f_path)
                compressed_files += 1
        return compressed_files
    
    def run(self):
        start = time.time()
        
        self.logger.log_info("Removing empty directories...")
        removed_dirs = self.rm_dirs()
        self.logger.log_info("Removed %d directories\n" % removed_dirs)

        self.logger.log_info("Compressing files bigger than %dMB..." % self.size)
        compressed_files = self.compress_files()
        self.logger.log_info("Compressed %d files\n" % compressed_files)

        # log duration
        duration_seconds = time.time() - start
        m, s = divmod(duration_seconds, 60)
        h, m = divmod(m, 60)
        self.logger.log_info("Done in %02d:%02d:%02d" % (h, m, s))
